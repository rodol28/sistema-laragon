@extends('layouts.app')

@section('content')
<h1> {{ $user->name }}</h1>
@foreach ($follows as $follow)
<ul class="list-unstyled">
<li> {{ $follow->username }}</li>
</ul>
@endforeach    
@endsection